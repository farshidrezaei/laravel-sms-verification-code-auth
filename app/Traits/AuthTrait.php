<?php


namespace App\Traits;


use App\Helpers\NumberHelper;
use App\Http\Requests\Auth\MobileCheckRequest;
use App\Http\Requests\Auth\VerificationCodeCheckRequest;
use App\Services\Responder;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait AuthTrait
{
    protected function getOrCreateUserByMobile( $mobile )
    {
        return User::where( 'mobile', $mobile )
            ->firstOr( static function () use ( $mobile ) {
                $user = User::create( [ 'mobile' => $mobile ] );
                $user->timestamps = false;
                return $user;
            } );
    }

    /**
     * @param $user
     *
     * @throws \Exception
     */
    protected function generateVerificationCode( User $user ): void
    {
        $user->verification_code()
            ->updateOrCreate( [ 'user_id' => $user->id ], [
                'code' => ( new NumberHelper )->verificationCodeGenerator(),
                'expired_at' => now()->addMinutes( 2 ),
            ] );
    }


    /**
     * @param $mobile
     *
     * @return User
     */
    protected function getUser( $mobile ): User
    {
        return User::whereMobile( $mobile )->firstOrFail();
    }


    protected function checkVerificationCodeAndLogin( $verificationCode, $user ): void
    {
        if ( $this->verificationCodeExpired( $user ) ) {
            responder()->message( app()->getLocale() === 'fa' ? 'کد تایید منقضی شده است.' : 'Verification code has expired.' )
                ->status( 401 )
                ->json()
                ->send();
        }

        if ( $this->isValidVerificationCode( $verificationCode, $user ) ) {
            $this->verifyUserMobile( $user );
            $this->handleUserLogin( $user );
        }
        responder()->message( app()->getLocale() === 'fa' ? 'کد تایید اشتباه است.' : 'Verification code is invalid.' )
            ->status( 401 )
            ->json()
            ->send();

    }

    /**
     * @param $user
     *
     * @return bool
     */
    protected function verificationCodeExpired( $user ): bool
    {
        return $user->verification_code->expired_at < now();
    }

    /**
     * @param VerificationCodeCheckRequest $verificationCode
     * @param                              $user
     *
     * @return bool
     */
    protected function isValidVerificationCode( $verificationCode, $user ): bool
    {
        return mb_strtoupper( $verificationCode ) === $user->verification_code->code;
    }

    /**
     * @param $user
     */
    protected function verifyUserMobile( User $user ): void
    {
        $user->timestamps = false;
        $user->save();
    }

    /**
     * @param           $user
     */
    protected function handleUserLogin( $user ): void
    {

        if ( !$token = auth()->login( $user ) ) {
            responder()->message( app()->getLocale() === 'fa' ? 'خطا' : 'Error' )
                ->status( 500 )
                ->json()
                ->send();
        }


        responder()->message( 'شما با موفقیت وارد سایت شدید.' )
            ->body( [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60,
            ] )
            ->status( 200 )
            ->json()
            ->send();
    }

    /**
     * @param           $user
     */
    protected function abortIfVerificationCodeHasNotBeenExpired( $user ): void
    {

        if ( $user->verification_code->expired_at > now() ) {
            responder()->message( app()->getLocale() === 'fa' ? 'کد قبلی هنوز منقضی نشده است.' : 'Last Verification Code has not been expired.' )
                ->status( 403 )
                ->json()
                ->send();
        }
    }

}
