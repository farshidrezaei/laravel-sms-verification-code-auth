<?php

namespace App\Http\Requests\Auth;

use App\Rules\VerificationCodeRule as VerificationCodeRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string verification_code
 * @property string mobile
 */
class VerificationCodeCheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'mobile' => 'required|numeric',
            'verification_code' => ['required', new VerificationCodeRule]

        ];
    }
}
