<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\MobileCheckRequest;
use App\Http\Requests\Auth\VerificationCodeCheckRequest;

use App\Http\Resources\User as UserResource;
use Exception;


class AuthController extends Controller
{
    use App\Traits\AuthTrait;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware( 'auth:api', [
            'except' => [
                'verify',
                'resendVerificationCode',
                'checkMobile',
                'checkVerificationCode',
            ],
        ] );
    }

    /**
     * @param MobileCheckRequest $request
     *
     * @throws Exception
     */
    public function checkMobile( MobileCheckRequest $request ): void
    {

        $user = $this->getOrCreateUserByMobile( $request->mobile );

        $this->generateVerificationCode( $user );

        $user->sendVerificationCodeSms();


        responder()->message( trans( 'messages.check_mobile' ) )
            ->body( [
                'verification_code' => env( 'APP_DEBUG' )
                    ? $user->verification_code->code
                    : null,
            ] )
            ->json()
            ->send();
    }


    /**
     * Get the authenticated User.
     *
     * @param
     *
     * @return void
     */
    public function user(): void
    {
        $user = new UserResource( auth()->user() );

        responder()->body( [ 'user' => $user ] )->json()->send();
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @param
     *
     * @return void
     */
    public function logout(): void
    {
        auth()->logout();
        responder()->message( 'Successfully logged out.' )->json()->send();
    }

    /**
     * Refresh a token.
     *
     * @param
     *
     * @return void
     */
    public function refresh(): void
    {
        responder()->message( 'Refresh Token Refreshed.' )
            ->body( [
                'access_token' => auth()->refresh(),
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60,
            ] )
            ->json()
            ->send();

    }

    /**
     * Get the token array structure.
     *
     * @param MobileCheckRequest $request
     *
     * @return void
     * @throws Exception
     */
    public function resendVerificationCode( MobileCheckRequest $request ): void
    {

        $user = $this->getUser( $request->mobile );
        $this->abortIfVerificationCodeHasNotBeenExpired( $user );
        $this->generateVerificationCode( $user );
        $user->save();
        $user->sendVerificationCodeSms();
        responder()->message( app()->getLocale() === 'fa' ? 'ایمیلی حاوی کد تایید جدید، مجددا برای شما ارسال شد.' : 'New verification code has sent to your mobile number.' )
            ->json()
            ->send();

    }

    /**
     * @param VerificationCodeCheckRequest $request
     *
     * @return void
     */
    public function checkVerificationCode( VerificationCodeCheckRequest $request ): void
    {
        $user = $this->getUser( $request->mobile );

        $this->checkVerificationCodeAndLogin( $request->verification_code, $user );
    }


}
