<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {

        Factory( App\User::class )
            ->create( [
                'name' => 'Mahna Vatani',
                'username' => 'mahnavatani',
                'mobile' => '09121234567',
                'password' => Hash::make( 'mahna' ),
            ] );


    }
}
