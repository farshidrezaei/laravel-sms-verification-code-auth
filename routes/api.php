<?php

use App\Article;
use App\Http\Controllers\ArticleController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix( 'v1' )
    ->group( static function () {
        Route::prefix( 'auth' )
            ->namespace( 'Auth' )
            ->middleware( 'api' )
            ->group( static function () {
                Route::post( 'check-mobile', 'AuthController@checkEmail' )->middleware( 'guest' );
                Route::post( 'check-verification-code', 'AuthController@checkVerificationCode' )->middleware( 'guest' );
                Route::post( 'logout', 'AuthController@logout' );
                Route::post( 'refresh', 'AuthController@refresh' );
                Route::get( 'user', 'AuthController@user' );
                Route::post( 'resend-verification-code', 'AuthController@resendVerificationCode' )->middleware( 'guest' );
            } );
    } );
